<?php
/**
 * Created 10.10.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PTW\Admin
 */

?>
<h1><?php esc_html_e( 'Settings', 'ptw' ); ?></h1>
<form method="post" action="options.php">
	<?php
	settings_fields( 'ptw_poster_settings_group' );
	do_settings_sections( 'ptw-settings' );
	?>
	<?php submit_button(); ?>
</form> 
