<?php
/**
 * Created 13.10.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 * Description: Working with Api Poster.
 * Documents Api: https://dev.joinposter.com/docs/v3/start/index
 *
 * @package PTW\Api
 */

namespace PTW\Api;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * PoserApi class file.
 */
class PoserApi {

	/**
	 * GuzzleHttp Client.
	 *
	 * @var Client Http Client.
	 */
	private $http;
	/**
	 * Options Token Poser API.
	 *
	 * @var false|mixed|void
	 */
	private $token;

	/**
	 * PoserApi construct.
	 */
	public function __construct() {
		$this->token = get_option( PTW_PREFIX . 'poster_settings_api' )['token'];
		$this->http  = new Client();
	}


	/**
	 * Send HTTP request to Api.
	 *
	 * @param string $point  Url break point.
	 * @param string $method Method POST|GET|PUT|DELETE.
	 *
	 * @return false|void
	 */
	public function getRequest( string $point, string $method ) {
		if ( empty( $this->token ) ) {
			return;
		}

		try {
			$data = json_decode(
				$this->http->request( $method, $point )
					->getBody()
					->getContents(),
				false,
				512,
				JSON_THROW_ON_ERROR
			);
		} catch ( GuzzleException $e ) {
			$this->logError( $e->getMessage() );
		} catch ( \JsonException $e ) {
			$this->logError( $e->getMessage() );
		}

		if ( ! empty( $data->response ) ) {
			return $data->response;
		}

		return false;
	}

	/**
	 * Get Url Point from API Poster.
	 *
	 * @param string   $point Point Name.
	 * @param int|null $id    ID in Poster.
	 *
	 * @return string
	 */
	public function getPoint( string $point, int $id = null ): string {
		switch ( $point ) {
			case 'products':
				return 'https://joinposter.com/api/menu.getProducts?token=' . $this->token . '&type=products';
			case 'product':
				return 'https://joinposter.com/api/menu.getProducts?token=' . $this->token . '&product_id=' . $id;
			case 'categories':
				return 'https://joinposter.com/api/menu.getCategories?token=' . $this->token;
			case 'category':
				return 'https://joinposter.com/api/menu.getCategories?token=' . $this->token . '&category_id=' . $id;
			case 'moves':
				return 'https://joinposter.com/api/storage.getMoves?token=' . $this->token;
			case 'move':
				return 'https://joinposter.com/api/storage.getMove?token=' . $this->token . '&move_id=' . $id;
			case 'storage_leftovers':
				return 'https://joinposter.com/api/storage.getStorageLeftovers?token=' . $this->token . '&type=2&zero_leftovers=true';
			default:
				return '';
		}
	}

	/**
	 * Debug mode.
	 *
	 * @param string $log error logs.
	 */
	public function logError( $log ): void {

		if ( true === WP_DEBUG ) {

			if ( is_array( $log ) || is_object( $log ) ) {
				// phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_error_log
				// phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_print_r
				error_log( print_r( $log, true ) );
				// phpcs:enable WordPress.PHP.DevelopmentFunctions.error_log_error_log
				// phpcs:enable WordPress.PHP.DevelopmentFunctions.error_log_print_r
			} else {
				// phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_error_log
				// phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_print_r
				error_log( $log );
				// phpcs:enable WordPress.PHP.DevelopmentFunctions.error_log_error_log
				// phpcs:enable WordPress.PHP.DevelopmentFunctions.error_log_print_r
			}
		} else {
			ob_start();

			echo '[' . esc_attr( gmdate( 'd-M-Y h:i:s T' ) ) . '] ';

			if ( is_array( $log ) || is_object( $log ) ) {
				// phpcs:disable WordPress.PHP.DevelopmentFunctions.error_log_print_r
				print_r( $log );
				// phpcs:enable WordPress.PHP.DevelopmentFunctions.error_log_print_r
			} else {
				echo esc_html( $log );
			}

			echo "\r\n";
			//phpcs:disable WordPress.WP.AlternativeFunctions.file_system_read_file_put_contents
			file_put_contents( ABSPATH . 'wp-content/ptw_debug.log', ob_get_clean(), FILE_APPEND );
			//phpcs:enable WordPress.WP.AlternativeFunctions.file_system_read_file_put_contents
		}
	}
}
