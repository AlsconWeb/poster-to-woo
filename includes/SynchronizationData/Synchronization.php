<?php
/**
 * Created 13.10.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PTW\SynchronizationData
 */

namespace PTW\SynchronizationData;

use PTW\Api\PoserApi;

/**
 * Synchronization class file.
 */
class Synchronization {
	/**
	 * Product Data from Api.
	 *
	 * @var array $productData .
	 */
	private array $productData;
	/**
	 * Poster Api Class.
	 *
	 * @var PoserApi $api ;
	 */
	private PoserApi $api;

	protected const LEFTOVERS     = 'leftovers';
	protected const INGREDIENT_ID = 'ingredient_id';

	public function __construct() {
		$this->api = new PoserApi();
	}

	/**
	 * Create Product Woocommerce.
	 *
	 * @param object $data data from api.
	 */
	public function createProduct( object $data ) {
		if ( empty( $data ) ) {
			return false;
		}

		$leftovers = $this->getStorageLeftovers();
		$stock     = (int) $this->getStorageLeftoversByID( (int) $data->ingredient_id, $leftovers )->ingredient_left;
		$price     = (array) $data->price;
		$price     = $price[1] / 100;

		$dataProd = [
			'post_status' => 'draft',
			'post_title'  => sanitize_text_field( $data->product_name ),
			'post_type'   => 'product',
			'post_author' => 1,
			'ping_status' => get_option( 'default_ping_status' ),
			'meta_input'  => [
				'_regular_price'     => $price,
				'_tax_status'        => 'taxable',
				'_manage_stock'      => 'yes',
				'_backorders'        => 'no',
				'_sold_individually' => 'no',
				'_virtual'           => 'no',
				'_downloadable'      => 'no',
				'_download_limit'    => - 1,
				'_download_expiry'   => - 1,
				'_stock'             => $stock,
				'_stock_status'      => 0 !== $stock ? 'instock' : 'outstock',
				'_thumbnail_id'      => '', // create function upload file.
				'_sku'               => $data->product_code ?? '',
			],
		];

		$prodWooID = 212;

		return $prodWooID;
	}

	/**
	 * Add to DB info about product.
	 *
	 * @param array $prodData  Product Poster.
	 * @param int   $prodWooID ID woocommerce product.
	 */
	private function addToDb( array $prodData, int $prodWooID ): void {
	}

	public function isProductInDb( $productID ): bool {
		global $wpdb;

		// phpcs:disable WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
		$result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}poser_products_sync WHERE po_id = %d", $productID ) );
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.NoCaching
		if ( $result ) {
			return true;
		}

		return false;
	}

	/**
	 * Synchronization Product.
	 */
	public function startSynchronizationProduct(): void {
		$productData = $this->api->getRequest( $this->api->getPoint( 'products' ), 'get' );
		if ( ! $productData ) {
			$this->api->logError( 'empty products' );

			return;
		}
		foreach ( $productData as $product ) {
			if ( $this->isProductInDb( $product->product_id ) ) {
				continue;
			}

			$prodWooID = $this->createProduct( $product );
//			$this->addToDb( $product, $prodWooID );
		}
	}

	/**
	 * Receive stock balances.
	 *
	 * @return false|mixed
	 */
	public function getStorageLeftovers() {
		$leftovers = get_transient( self::LEFTOVERS );
		if ( false === $leftovers ) {
			$leftovers = $this->api->getRequest( $this->api->getPoint( 'storage_leftovers' ), 'get' );

			if ( empty( $leftovers ) ) {
				$this->api->logError( '$leftovers->empty' );

				return false;
			}

			set_transient( self::LEFTOVERS, $leftovers, DAY_IN_SECONDS );

			return $leftovers;
		}

		return $leftovers;
	}

	/**
	 *  Returning from the general remainder data array by ingredient id.
	 *
	 * @param int   $poserID   Ingredient ID.
	 * @param array $leftovers Array obtained from Api warehouse leftovers.
	 *
	 * @return false|mixed
	 */
	public function getStorageLeftoversByID( int $poserID, array $leftovers ) {

		if ( empty( $leftovers ) ) {
			return false;
		}

		$prodIndex = array_search( $poserID, array_column( $leftovers, self::INGREDIENT_ID ) );

		return $leftovers[ $prodIndex ];

	}
}

