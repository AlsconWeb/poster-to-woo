<?php
/**
 * Created 10.10.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PTW\Init
 */

namespace PTW\Init;

use PTW\Admin\Settings;
use PTW\SynchronizationData\Synchronization;

/**
 * Class File.
 */
class PluginInit {

	/**
	 * PluginInit construct.
	 */
	public function __construct() {
		add_action( 'admin_enqueue_scripts', [ $this, 'addAdminScript' ] );
		add_action( 'init', [ $this, 'test' ] );
		new Settings();
	}

	public function test(): void {
		$sync = new Synchronization();
		$sync->startSynchronizationProduct();
	}

	/**
	 * Check php version.
	 *
	 * @return bool
	 * @noinspection ConstantCanBeUsedInspection
	 */
	public static function is_php_version(): bool {
		if ( version_compare( constant( 'PTW_MINIMUM_PHP_REQUIRED_VERSION' ), phpversion(), '>' ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Return Version file.
	 *
	 * @param string $src The relative path to the file in the theme.
	 *
	 * @return false|int
	 */
	private static function versionFile( string $src ) {
		return filemtime( PCS_PATH . $src );
	}

	/**
	 * Add Script and Style to admin panel.
	 *
	 * @param string $hook Hook name page in admin.
	 */
	public function addAdminScript( string $hook ): void {

	}

	/**
	 * Create Custom table poser_customer_sync.
	 */
	public static function createCustomTable(): void {
		global $wpdb;

		$prefix = $wpdb->base_prefix;
		// phpcs:disable WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
		// phpcs:disable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
		$table = $wpdb->get_results( "SHOW TABLES LIKE '{$prefix}poser_products_sync '" );

		if ( ! $table ) {
			$sql = "CREATE TABLE `{$prefix}{PTW_PREFIX}poser_products_sync` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `wp_id` BIGINT NULL , `po_id` BIGINT NULL , `po_cat_id` BIGINT NULL , `po_leftovers` INT NULL , PRIMARY KEY (`id`));";

			include_once ABSPATH . 'wp-admin/includes/upgrade.php';

			dbDelta( $sql );
		}
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.NoCaching
		// phpcs:enable WordPress.DB.DirectDatabaseQuery.DirectQuery
		// phpcs:enable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
	}
}
