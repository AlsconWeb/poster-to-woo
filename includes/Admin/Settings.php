<?php
/**
 * Created 14.09.2021
 * Version 1.0.0
 * Last update
 * Author: Alex L
 * Author URL: https://i-wp-dev.com/
 *
 * @package PCS\Admin
 */

namespace PTW\Admin;

/**
 * SettingsPage class file.
 */
class Settings {
	/**
	 * Construct class SettingsPage.
	 */
	public function __construct() {
		add_action( 'admin_menu', [ $this, 'registerSettingsPage' ] );
		add_action( 'admin_init', [ $this, 'registerSettings' ] );
	}

	/**
	 * Register Settings Page.
	 */
	public function registerSettingsPage(): void {
		add_menu_page(
			__( 'Settings', 'pcs' ),
			__( 'Settings Poster to Woo', 'pcs' ),
			'manage_options',
			'ptw-settings',
			[
				$this,
				'addSettingsPage',
			],
			'dashicons-admin-generic',
			90
		);
	}

	/**
	 * Output HTML settings page.
	 */
	public function addSettingsPage(): void {
		ob_start();
		include_once PTW_PATH . '/template/admin/settings-page.php';
		// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		echo ob_get_clean();
		// phpcs:enable WordPress.Security.EscapeOutput.OutputNotEscaped
	}

	/**
	 * Register Options settings.
	 */
	public function registerSettings(): void {
		register_setting( 'ptw_poster_settings_group', 'ptw_poster_settings_api' );
		add_settings_section( PTW_PREFIX . 'poster_token', __( 'API Settings', 'ptw' ), '', 'ptw-settings' );
		add_settings_field(
			PTW_PREFIX . 'poster_token',
			__( 'Token', '' ),
			[
				$this,
				'tokenSettings',
			],
			'ptw-settings',
			PTW_PREFIX . 'poster_token'
		);
	}

	/**
	 * Token input.
	 */
	public function tokenSettings(): void {
		$token = get_option( PTW_PREFIX . 'poster_settings_api' );
		// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
		echo "<input id='token_poster' name='" . PTW_PREFIX . 'poster_settings_api' . "[token]' type='text' value='" . esc_attr( $token['token'] ?? '' ) . "' />";
		// phpcs:enable WordPress.Security.EscapeOutput.OutputNotEscaped
	}

}

