<?php
/**
 * Poster to Woocommerce.
 *
 * @author            Alex L
 * @license           GPL-2.0-or-later
 * @wordpress-plugin
 * @package           PTW
 *
 * Plugin Name:       Poster to Woocommerce
 * Plugin URI:        https://i-wp-dev.com/
 * Description:       Integrates with the Poster system. Synchronizes product and warehouse.
 * Version:           1.0.0a
 * Requires at least: 5.0.0
 * Requires PHP:      7.4
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       ptw
 * Domain Path:       /languages/
 */

/**
 * Plugin version.
 */

use PTW\Init\PluginInit;

const PTW_VERSION = '1.0.0a';

/**
 * Path to the plugin dir.
 */
const PTW_PATH = __DIR__;

/**
 * Plugin dir url.
 */
define( 'PTW_URL', untrailingslashit( plugin_dir_url( __FILE__ ) ) );

/**
 * Plugin prefix.
 */
const PTW_PREFIX = 'pcs_';

/**
 * Minimum required php version.
 */
const PTW_MINIMUM_PHP_REQUIRED_VERSION = '7.4';

/**
 * Main plugin file.
 */
const PTW_FILE = __FILE__;

/**
 * Init plugin on plugin load.
 */
require_once constant( 'PTW_PATH' ) . '/vendor/autoload.php';

if ( ! PluginInit::is_php_version() ) {

	add_action( 'admin_notices', 'PTW\Admin\AdminNotice::php_version_nope' );

	if ( is_plugin_active( plugin_basename( constant( 'PTW_FILE' ) ) ) ) {
		deactivate_plugins( plugin_basename( constant( 'PTW_FILE' ) ) );
		// phpcs:disable WordPress.Security.NonceVerification.Recommended
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}

	return;
}

if ( ! is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
	add_action( 'admin_notices', 'PTW\Admin\AdminNotice::woocommerce_active' );
}

register_activation_hook( __FILE__, 'activation_ptw' );

/**
 * Activation Poster to WooCommerce plugin.
 */
function activation_ptw() {
	PluginInit::createCustomTable();
}

global $ptw_plugin;
$ptw_plugin = new PluginInit();